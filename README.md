# MogDB Kettle Database plugin


## How to build 

### Pre-requisites for building the project:

* Maven, version 3+
* Java JDK 11
* This [settings.xml](https://raw.githubusercontent.com/pentaho/maven-parent-poms/master/maven-support-files/settings.xml) in your <user-home>/.m2 directory

### Building it

This is a Maven project, and to build it use the following command:

```
$ mvn clean install
```
Optionally you can specify -Drelease to trigger obfuscation and/or uglification (as needed)

Optionally you can specify -Dmaven.test.skip=true to skip the tests (even though
you shouldn't as you know)

The build result will be a Pentaho package located in ```target```.

### Packaging / Distributing it

Packages can be built by using the following command:
```
$ mvn clean package
```

The packaged results will be in the `target/`.

For example, a distribution can then be found in: `target/kettel-database-mogdb-*.zip`.

### How to install 

Copy the plugin file into data-integration/plugins/ and unzip,  After restarting the PDI client, the new database type is available from the PDI database dialog box.

```shell
cp kettel-database-mogdb-*.zip data-integration/plugins/
cd data-integration/plugins/
unzip kettel-database-mogdb-*.zip
ls -l plugins
drwxr-xr-x  4 travel  staff     128 11 28 00:57 kettle-database-mogdb-1.0.0
-rw-r--r--@ 1 travel  staff  877711 11 28 00:57 kettle-database-mogdb-1.0.0.zip


ls -l kettle-database-mogdb-1.0.0
total 8
-rw-r--r--  1 travel  staff  3530 11 28 00:57 kettle-database-mogdb-1.0.0.jar
drwxr-xr-x  3 travel  staff    96 11 28 00:57 lib # database driver
```

![New Connection](./img/mogdb.png)

### 

